# Cogito

Cogito provides high-quality data sets for machine learning and AI-supported systems and business applications with specialization in data collection, classification and data enrichment with image annotation and data labeling services for different industries.